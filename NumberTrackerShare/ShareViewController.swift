//
//  ShareViewController.swift
//  NumberTrackerShare
//
//  Created by James Cash on 03-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import Social
import NumberTrackerUtils

class ShareViewController: SLComposeServiceViewController {

    override func isContentValid() -> Bool {
        // Do validation of contentText and/or NSExtensionContext attachments here
        let fmt = NumberFormatter()
        fmt.allowsFloats = false
        fmt.minimum = 0
        fmt.maximum = 100

        if fmt.number(from: contentText) != nil {
            return true
        } else {
            return false
        }
    }

    override func didSelectPost() {
        // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
    
        // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.

        let number = Int(contentText)! // safe, because we checked isContentValid
        saveNumber(number)

        self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
    }

    override func configurationItems() -> [Any]! {
        // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
        return []
    }

}
