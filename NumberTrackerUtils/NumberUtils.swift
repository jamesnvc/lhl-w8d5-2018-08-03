//
//  NumberUtils.swift
//  NumberTracker
//
//  Created by James Cash on 03-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import Foundation

let defaultsKey = "03-08-18-numberKey"
let defaultsSuite = "group.occasionallycogent.testing"

// things that we want to be usable from this shared framework need to be public
public func saveNumber(_ theNumber: Int) {
    // instead of UserDefaults.standard
    UserDefaults(suiteName: defaultsSuite)!.set(theNumber, forKey: defaultsKey)
}

public func restoreNumber() -> Int {
    return UserDefaults(suiteName: defaultsSuite)!.integer(forKey: defaultsKey)
}
