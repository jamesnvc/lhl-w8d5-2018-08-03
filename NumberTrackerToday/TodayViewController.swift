//
//  TodayViewController.swift
//  NumberTrackerToday
//
//  Created by James Cash on 03-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import NotificationCenter
import NumberTrackerUtils

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet var numberStepper: UIStepper!
    @IBOutlet var numberLabel: UILabel!

    var theNumber: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        numberLabel.text = "\(theNumber)"
        numberStepper.value = Double(theNumber)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func steppedNumber(_ sender: UIStepper) {
        theNumber = Int(sender.value)
        numberLabel.text = "\(theNumber)"
        saveNumber(theNumber)
    }

    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData

        theNumber = restoreNumber()
        numberLabel.text = "\(theNumber)"
        numberStepper.value = Double(theNumber)

        completionHandler(NCUpdateResult.newData)
    }
    
}
