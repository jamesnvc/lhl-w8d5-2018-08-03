//
//  ViewController.swift
//  NumberTracker
//
//  Created by James Cash on 03-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import NumberTrackerUtils

class ViewController: UIViewController {

    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var numberSlider: UISlider!

    var theNumber: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // we actually want to make sure we restore whenever the data changes...which is hard to do in general
        theNumber = restoreNumber()
        numberSlider.value = Float(theNumber)
        numberLabel.text = "\(theNumber)"
    }

    @IBAction func numberChanged(_ sender: UISlider) {
        theNumber = Int(sender.value)
        numberLabel.text = "\(theNumber)"
        saveNumber(theNumber)
    }


}

